<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
  private $users = [
    ['name' => 'user1', 'email' => 'user1@domaine.com', 'password' => 'user1pwd'],
    ['name' => 'user2', 'email' => 'user2@domaine.com', 'password' => 'user2pwd'],
    ['name' => 'user3', 'email' => 'user3@domaine.com', 'password' => 'user3pwd']
  ];


  public function myAction () {
	$users=$this->users;
	return view ('liste ',['users'=>$users]);

	}
  
}